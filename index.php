<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>
<body>
    <section class="section">

        <form id="formContact" class="box">
            <div class="control">
                <label class="radio">
                    <input value="M" type="radio" name="salutation" >
                    M.
                </label>
                <label class="radio">
                    <input value="Mme" type="radio" name="salutation" >
                    Mme.
                </label>
                <p id="salutation-status" class="help has-text-danger"></p>
            </div>

            <div class="field">
                <label class="label">Pseudo</label>
                <div class="control">
                    <input name="username" class="input" type="text" placeholder="Entrer votre pseudo" >
                </div>
                <p id="username-status" class="help has-text-danger"></p>
            </div>

            <div class="field">
                <label class="label">Mot de passe</label>
                <div class="control">
                    <input name="password" class="input" type="password" placeholder="Entrer votre mot de passe" >
                </div>
                <p id="password-status" class="help has-text-danger"></p>
            </div>

            <div class="field">
                <label class="label">Confirmer votre mot de passe</label>
                <div class="control">
                    <input name="passwordConfirm" class="input" type="password" placeholder="Confirmer votre mot de passe" >
                </div>
                <p id="passwordConfirm-status" class="help has-text-danger"></p>
            </div>

            <div class="field">
                <label class="label">Email</label>
                <div class="control">
                    <input name="email" class="input" type="text" placeholder="Entrer votre email" >
                </div>
                <p id="email-status" class="help has-text-danger"></p>
            </div>

            <div class="field">
                <label class="label">Pays</label>
                <div class="control">
                    <div class="select">
                        <select name="country" >
                            <option value="">Séléctionner un pays</option>
                            <option value="CH">Suisse</option>
                            <option value="FR">France</option>
                            <option value="IT">Italie</option>
                            <option value="DE">Allemagne</option>
                        </select>
                    </div>
                </div>
                <p id="country-status" class="help has-text-danger"></p>
            </div>

            <div class="field">
                <div class="control">
                    <label class="checkbox">
                        <input name="agree" type="checkbox" >
                        I agree to the <a href="#">terms and conditions</a>
                    </label>
                </div>
                <p id="agree-status" class="help has-text-danger"></p>
            </div>

            <p id="send-success" class="has-text-centered has-text-success"></p>

            <div class="field is-grouped">
                <div class="control">
                    <button type="submit" class="button is-link">Envoyer</button>
                </div>
            </div>
        </form>
    </section>

    <script src="index.js"></script>
</body>
</html>