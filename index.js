const form = document.getElementById("formContact");


form.addEventListener("submit", async e => {
    e.preventDefault();

    const body = new FormData(form);
    body.append("contactForm","contactForm");

    const errors = document.getElementsByClassName("has-text-danger");

    Array.from(errors).forEach((el) => {
        el.textContent = "";
    });

    document.getElementById("send-success").textContent ="";


    const req = await fetch("validation_form.php", {
        method: "POST",
        headers: {
            "Accept": "application/json"
        },
        body
    })

    const res = await req.json();

    if (req.status === 400){
        Object.keys(res.message).forEach((e) => {
            console.log(e,res.message[e]);
            document.getElementById(e+"-status").textContent = res.message[e];
        })
    }else{
        document.getElementById("send-success").textContent = res.message;
    }
})