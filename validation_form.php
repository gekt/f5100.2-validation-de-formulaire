<?php

    if ($_SERVER["REQUEST_METHOD"] === "POST"){

        if (!empty($_POST["contactForm"])) {

            $arrErrors = [];

            $salutation = isset($_POST["salutation"]) ? $_POST["salutation"] : false;
            $username = (string)$_POST["username"];
            $password = (string)$_POST["password"];
            $passwordConfirm = (string)$_POST["passwordConfirm"];
            $email = (string)$_POST["email"];
            $country = (string)$_POST["country"];
            $agree = isset($_POST["agree"]);

            if (!$salutation) {
                $arrErrors["salutation"] = "Veuillez choisir !";
            }

            if (!$agree) {
                $arrErrors["agree"] = "Veuillez accepter les conditions d'utilisation !";
            }

            foreach ($_POST as $k => $v) {

                //TODO: CHECK PASSWORD REGEX

                if ($k === "email"){
                    if (!filter_var($v,FILTER_VALIDATE_EMAIL)){
                        $arrErrors[$k] = "Email incorrect !";
                    }
                }

                if (empty($v)) {
                    $arrErrors[$k] = "Veuillez remplir le champs correctement !";
                }
            }

            if ($passwordConfirm !== $password){
                $arrErrors["password"] = "Les mots de passe ne corresponent pas !";
                $arrErrors["passwordConfirm"] = "Les mots de passe ne corresponent pas !";
            }

            if (count($arrErrors) > 0){
                header('HTTP/1.1 400 Method not allowed');
                echo json_encode([
                    "error" => true,
                    "message" => $arrErrors
                ]);
                exit();
            }else{
                echo json_encode([
                   "error" => false,
                   "message" => "Formulaire envoyé !"
                ]);
            }
        }
    }else{
        header('HTTP/1.1 405 Method not allowed');
        exit();
    }